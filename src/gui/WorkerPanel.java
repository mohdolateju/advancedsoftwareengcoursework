package gui;


import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import patterns.WorkerObserver;
import main.Worker;

/**Represents A Worker JPanel Component*/
public class WorkerPanel implements WorkerObserver  {

	private static final int CENTER = 0;
	private String report;
	private JPanel workersPanel;
	private JButton addWorkerBtn, processParcelBtn,clearBtn;
	private JLabel WorkerBreakBtn;
	private Worker anotherworker;
	private JTextArea parcelTextarea;
	private JDialog dialog;
	public JDialog getDialog() {
		return dialog;
	}

	public void setDialog(JDialog dialog) {
		this.dialog = dialog;
	}

	private JButton cancelButton;
	public JButton getCancelButton() {
		return cancelButton;
	}

	public void setCancelButton(JButton cancelButton) {
		this.cancelButton = cancelButton;
	}

	private JButton okButton;
	
	public JButton getOkButton() {
		return okButton;
	}

	public void setOkButton(JButton okButton) {
		this.okButton = okButton;
	}

		//components of the AddWorker Dialog
		private JTextField nameTextField;
		private JTextField workerIDTextField;
	
	
	public JButton getAddWorkerBtn() {
		return addWorkerBtn;
	}

	public void setAddWorkerBtn(JButton addWorkerBtn) {
		this.addWorkerBtn = addWorkerBtn;
	}

	public JButton getProcessParcelBtn() {
		return processParcelBtn;
	}

	public void setProcessParcelBtn(JButton processParcelBtn) {
		this.processParcelBtn = processParcelBtn;
	}

	public JLabel getWorkerBreakBtn() {
		return WorkerBreakBtn;
	}

	public void setWorkerBreakBtn(JLabel workerBreakBtn) {
		WorkerBreakBtn = workerBreakBtn;
	}

	public JButton getClearBtn() {
		return clearBtn;
	}

	public void setClearBtn(JButton clearBtn) {
		this.clearBtn = clearBtn;
	}

	public JTextArea getParcelTextarea() {
		return parcelTextarea;
	}

	public void setParcelTextarea(JTextArea parcelTextarea) {
		this.parcelTextarea = parcelTextarea;
	}

	/**Initializes the Worker panel from a WorkerList
	 * 
	 * @param Worker List
	 */
	public WorkerPanel(Worker worker){
		this.workersPanel=new JPanel(new GridBagLayout());
		workersPanel.setOpaque(true);
		this.anotherworker=worker;
		anotherworker.registerObserver(this);		
	}
	
	public synchronized String WritetoGUI(Worker worker)
	{		
		this.anotherworker=worker;
		return report += worker.getStatus();
		
	}
	
	/** Gets the Worker Panel as a JPanel Component
	 * @return Worker JPanel Component*/
	public JPanel getPanel(){
		
		//Setting the Title of the JPanel as a JLabel
				JLabel title=new JLabel("WORKER'S PANEL", CENTER);
				GridBagConstraints titleConstraints=new GridBagConstraints();	
				titleConstraints.fill=GridBagConstraints.HORIZONTAL;
				titleConstraints.gridx=0;
				titleConstraints.gridy=0;
				titleConstraints.gridwidth=3;
				titleConstraints.ipady=5;				
				this.workersPanel.add(title, titleConstraints);
		
				
				
				//this.processParcelBtn.addActionListener(this);
				
				//this.processParcelBtn.setHorizontalAlignment(CENTER);
				GridBagConstraints processConstraints=new GridBagConstraints();	
				processConstraints.fill=GridBagConstraints.HORIZONTAL;
				processConstraints.gridx=0;
				processConstraints.gridy=1;
				processConstraints.gridwidth=1;
				titleConstraints.ipady=5;				
				this.workersPanel.add(processParcelBtn, processConstraints);
				
				//this.WorkerBreakBtn=new JButton("WORKERS' BREAK TIME");
				//this.WorkerBreakBtn.setHorizontalAlignment(CENTER);
				GridBagConstraints workerConstraints=new GridBagConstraints();	
				workerConstraints.fill=GridBagConstraints.HORIZONTAL;
				workerConstraints.gridx=1;
				workerConstraints.gridy=1;
				workerConstraints.gridwidth=1;
				//workerConstraints.ipady=5;				
				this.workersPanel.add(WorkerBreakBtn, workerConstraints);
				
				
				//this.WorkerBreakBtn.setHorizontalAlignment(CENTER);
				GridBagConstraints clearConstraints=new GridBagConstraints();	
				clearConstraints.fill=GridBagConstraints.HORIZONTAL;
				clearConstraints.gridx=2;
				clearConstraints.gridy=1;
				clearConstraints.gridwidth=1;
				//workerConstraints.ipady=5;				
				this.workersPanel.add(clearBtn, clearConstraints);
				//this.clearBtn.addActionListener(this);
				
				
						
		 parcelTextarea= new JTextArea("");
		//parcelTextarea.setBackground(Color.YELLOW);
		this.workersPanel.add(parcelTextarea);
		
		
		
		//list panel is put in the a scroll pane to make it scrollable
		JScrollPane ListScrollpane=new JScrollPane(parcelTextarea);
		ListScrollpane.setPreferredSize(new Dimension(600,400));		
		ListScrollpane.setMinimumSize(new Dimension(600,200));
		
		//setting rules for the List scroll pane
		GridBagConstraints ListConstraints=new GridBagConstraints();
		ListConstraints.fill=GridBagConstraints.HORIZONTAL;
		ListConstraints.gridx=0;
		ListConstraints.gridy=2;
		ListConstraints.gridwidth=3;
		
		this.workersPanel.add(ListScrollpane,ListConstraints);
		
		
		this.addWorkerBtn.setHorizontalAlignment(CENTER);
		ListConstraints.fill=GridBagConstraints.HORIZONTAL;
		ListConstraints.gridx=0;
		ListConstraints.gridy=3;
		ListConstraints.gridwidth=3;		
		workersPanel.add(addWorkerBtn,ListConstraints);
		
		
		return workersPanel;
	}

	

	@Override
	public void update() {				
		parcelTextarea.setText(anotherworker.getReport());
	}
	
	void addprocessParcelBtnListener(ActionListener al) {
		this.processParcelBtn=new JButton("PROCESS PARCEL");
		processParcelBtn.addActionListener(al);
    }
	
	void addprocessClearBtnListener(ActionListener al) {
		this.clearBtn=new JButton("CLEAR");
		clearBtn.addActionListener(al);
    }
	
	void addWorkerBtnListener(ActionListener al) {
		this.addWorkerBtn=new JButton("ADD NEW WORKER");
		addWorkerBtn.addActionListener(al);
    }
	
	void cancelButtonListener(ActionListener al) {
		this.cancelButton=new JButton("CANCEL");
		cancelButton.addActionListener(al);
    }
	
	void okButtonListener(ActionListener al) {
		this.okButton=new JButton("OK");
		okButton.addActionListener(al);
    }
	
	void WorkerBreakBtnListener(ActionListener al) {
		this.WorkerBreakBtn=new JLabel("                          ");
		//WorkerBreakBtn.addActionListener(al);
    }
	
	
	
	/**Displays Dialog For Adding Worker's Details*/
	public void addWorkerDialog(){		
		this.dialog=new JDialog(new JFrame(), "Add Worker", true);
		
		//Panel to Customise Dialog
		JPanel dialogpanel=new JPanel(new GridBagLayout());
		
		//Configuring Constraints
		GridBagConstraints dialogConstraints=new GridBagConstraints();			
		dialogConstraints.gridx=0;
		dialogConstraints.gridy=0;
		dialogConstraints.gridwidth=2;
		
		//Adding Message Label
		JLabel message=new JLabel("Please Enter Worker Details");
		dialogpanel.add(message, dialogConstraints);
		
		//Adding Name Label
		JLabel nameLabel=new JLabel("Name");
		dialogConstraints.gridx=0;
		dialogConstraints.gridy=1;
		dialogConstraints.gridwidth=1;
		dialogpanel.add(nameLabel, dialogConstraints);
		
		//Adding Name Text Field
		nameTextField=new JTextField();
		dialogConstraints.fill=GridBagConstraints.HORIZONTAL;
		dialogConstraints.gridx=1;
		dialogConstraints.gridy=1;		
		dialogpanel.add(nameTextField, dialogConstraints);
		
		
		//Adding ParcelID Text Field
		workerIDTextField=new JTextField();
		dialogConstraints.gridx=1;
		dialogConstraints.gridy=2;
		//dialogpanel.add(workerIDTextField, dialogConstraints);
		
		//Adding Cancel Button
		dialogConstraints.gridx=0;
		dialogConstraints.gridy=3;		
		dialogpanel.add(this.cancelButton, dialogConstraints);
		
		//Adding Adding OK Button
		dialogConstraints.gridx=1;
		dialogConstraints.gridy=3;		
		dialogpanel.add(this.okButton, dialogConstraints);
		
		//Adding panel to Dialog				
		this.dialog.add(dialogpanel);
		
		//Final Dialog Configuration
		this.dialog.pack();
		this.dialog.setSize(250, 150);
		this.dialog.setLocationRelativeTo(null);
		this.dialog.setVisible(true);	
	}

	public JTextField getNameTextField() {
		return nameTextField;
	}

	public void setNameTextField(JTextField nameTextField) {
		this.nameTextField = nameTextField;
	}

	public JTextField getWorkerIDTextField() {
		return workerIDTextField;
	}

	public void setWorkerIDTextField(JTextField workerIDTextField) {
		this.workerIDTextField = workerIDTextField;
	}
	
}
