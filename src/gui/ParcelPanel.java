package gui;


import lists.ParcelList;
import main.EventsLogger; 
import base.Parcel;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JPanel;
import java.util.HashMap;
import exceptions.ParcelFormatException;
import exceptions.ParcelIDException;
import java.util.ArrayList;

/**Represents A Parcel JPanel Component*/
public class ParcelPanel implements ActionListener{

	private static final int CENTER = 0;
	private ParcelList parcels;
	private JPanel parcelPanel, listPanel;
	private JDialog newParcelDialog;
	private JButton addParcelBtn, OKbtn, cancelBtn;
	private JTextField PIDtxt, weightTxt, lengthTxt, heightTxt, widthTxt, daysTxt;
	private ArrayList<JTextField> textfields;
	//private static Logger logger =Logger.getLogger("AppsLog");
    //FileHandler fh;
	private EventsLogger eventlogger=EventsLogger.getInstance();
	
	//A Hash map Reference to the Parcel List GUI components
		private HashMap <String,JTextField> ParcelIdInDepot=new HashMap <String,JTextField>();
		private HashMap <String,JTextField> ParcelDimension=new HashMap <String,JTextField>();
		private HashMap <Integer,JTextField> ParcelDaysInDepot=new HashMap <Integer,JTextField>();
	
	String parcelParameter,unprocessedParcel;
	
	
	//Last Line Number of the GUI
		private int lineNo;
	
	/**Initializes the Parcel panel from a ParcelList
	 * @param Parcel List
	 */
	public ParcelPanel(ParcelList parcels){
		
		this.parcels=parcels;
		//this.parcelPanel=new JPanel(new FlowLayout());
		textfields=new ArrayList<JTextField>();
		this.parcelPanel=new JPanel(new GridBagLayout());
		

	}
	
	/**Adds A New Parcel To the List Panel
	   @param New Parcel Object*/
	private void addNewParcelToPanel(Parcel parcel){
		
		//getting details of the Parcel
		String Id=parcel.getParcelID();
		String dimension=parcel.getDimension();
		int daysInDepot=parcel.getDaysInDepot();
							
		//displaying the sequence number of the Parcel in a disabled text field
		JTextField ParcelId=new JTextField();
		ParcelId.setHorizontalAlignment(CENTER);
		ParcelId.setText(" "+Id+" ");
		ParcelId.setEnabled(false);
		ParcelId.setDisabledTextColor(Color.BLACK);
		GridBagConstraints parcelConstraints=new GridBagConstraints();
		parcelConstraints.fill=GridBagConstraints.HORIZONTAL;
		parcelConstraints.gridx=0;			
		parcelConstraints.gridy=lineNo;
		parcelConstraints.ipadx=12;
		listPanel.add(ParcelId, parcelConstraints);
		
		textfields.add(ParcelId);
		//creating a reference to the text field
		ParcelIdInDepot.put(Id, ParcelId);
		
		//displaying the Dimension of Parcel in a disabled text field
				JTextField parcelDimension=new JTextField();
				parcelDimension.setHorizontalAlignment(CENTER);
				parcelDimension.setText(dimension);
				parcelDimension.setEnabled(false);
				parcelDimension.setDisabledTextColor(Color.BLACK);
				parcelConstraints.gridx=1;
				listPanel.add(parcelDimension, parcelConstraints);
				
				textfields.add(parcelDimension);
				//creating a reference to the text field
				ParcelDimension.put(dimension, parcelDimension);
				
				//displaying the Days of the parcel in Depot in a disabled text field
				JTextField parceldays=new JTextField();
				parceldays.setHorizontalAlignment(CENTER);
				parceldays.setText(" "+daysInDepot+" ");
				parceldays.setEnabled(false);				
				parceldays.setDisabledTextColor(Color.BLACK);
				parcelConstraints.gridx=2;			
				listPanel.add(parceldays, parcelConstraints);
				
				textfields.add(parceldays);
				//creating a reference to the text field
				ParcelDaysInDepot.put(daysInDepot, parceldays);
	
		//Line number increased by 1
		lineNo++;
	}
	
	/** Gets the Parcel Panel as a JPanel Component
	 * @return Parcel JPanel Component*/
	public JPanel getPanel(){
		
		//Setting the Title of the JPanel as a JLabel
				JLabel parcelParameter=new JLabel("PARCEL LIST", CENTER);
				GridBagConstraints titleConstraints=new GridBagConstraints();	
				titleConstraints.fill=GridBagConstraints.HORIZONTAL;
				titleConstraints.gridx=0;
				titleConstraints.gridy=0;
				titleConstraints.gridwidth=3;
				titleConstraints.ipady=5;				
				this.parcelPanel.add(parcelParameter, titleConstraints);
				
				//Setting the Parcel Id Label
				JLabel IdTitle=new JLabel("PARCEL ID");
				GridBagConstraints IdConstraints=new GridBagConstraints();
				IdConstraints.gridx=0;
				IdConstraints.gridy=1;
				IdConstraints.ipadx=8;
				this.parcelPanel.add(IdTitle, IdConstraints);
				
				//Setting the Parcel Dimension Label
				JLabel domensionTitle=new JLabel("DIMENSION", CENTER);
				GridBagConstraints domensionTitleConstraints=new GridBagConstraints();	
				domensionTitleConstraints.gridx=1;
				domensionTitleConstraints.gridy=1;
				domensionTitleConstraints.ipadx=8;
				this.parcelPanel.add(domensionTitle, domensionTitleConstraints);
				
				//Setting the Status Label
				JLabel StatusTitle=new JLabel("DAYS", CENTER);
				GridBagConstraints statusConstraints=new GridBagConstraints();
				statusConstraints.fill=GridBagConstraints.HORIZONTAL;
				statusConstraints.gridx=2;
				statusConstraints.gridy=1;
				statusConstraints.ipadx=0;
				this.parcelPanel.add(StatusTitle, statusConstraints);
				
				//JPanel for the List of Customers
				listPanel=new JPanel(new GridBagLayout());
				
				//The line number for the start of the Parcel details used in the loop
				lineNo=3;
				
				//Loop for displaying all customers
				for(Parcel parcel: parcels.getParcelList()){
					
					addNewParcelToPanel(parcel);
				}
				
				//list panel is put in the a scroll pane to make it scrollable
				JScrollPane ListScrollpane=new JScrollPane(listPanel);
				ListScrollpane.setPreferredSize(new Dimension(200,400));
				ListScrollpane.setMinimumSize(new Dimension(150,200));
				
				//setting rules for the List scroll pane
				GridBagConstraints ListConstraints=new GridBagConstraints();
				ListConstraints.fill=GridBagConstraints.HORIZONTAL;
				ListConstraints.gridx=0;
				ListConstraints.gridy=2;
				ListConstraints.gridwidth=3;
				
				this.parcelPanel.add(ListScrollpane,ListConstraints);
				
				//Adding the "new parcel" button to 
				this.addParcelBtn=new JButton("ADD NEW PARCEL");
				this.addParcelBtn.setHorizontalAlignment(CENTER);
				this.addParcelBtn.addActionListener(this);
				GridBagConstraints addCustomerConstraint=new GridBagConstraints();
				addCustomerConstraint.fill=GridBagConstraints.HORIZONTAL;
				addCustomerConstraint.gridx=0;
				addCustomerConstraint.gridy=3;
				addCustomerConstraint.gridwidth=3;
						
				this.parcelPanel.add(this.addParcelBtn,addCustomerConstraint);
		
		
		return parcelPanel;
	}
	
	public void newParcel()
	{
		
		this.newParcelDialog = new JDialog (new JFrame(), "ADD PARCEL", true);
		
		JPanel dialogpanel = new JPanel(new GridLayout(8,2));
		dialogpanel.setSize(50, 50);
		
		OKbtn = new JButton("OK");
		OKbtn.addActionListener(this);
		
		cancelBtn = new JButton("Cancel");
		cancelBtn.addActionListener(this);
		
		JLabel enterPID = new JLabel("Enter ID");
		PIDtxt = new JTextField();
		
		JLabel enterWeight = new JLabel("Enter Weight");
		weightTxt = new JTextField();
		
		JLabel enterLength = new JLabel("Enter Length");
		lengthTxt = new JTextField();
		
		JLabel enterWidth = new JLabel("Enter Width");
		widthTxt = new JTextField();
		
		JLabel enterHeight = new JLabel("Enter Height");
		heightTxt = new JTextField();
		
		JLabel enterDays = new JLabel("Enters Days in depot");
		daysTxt = new JTextField();
		
		dialogpanel.add(enterPID); dialogpanel.add(PIDtxt);
		dialogpanel.add(enterWeight); dialogpanel.add(weightTxt);
		dialogpanel.add(enterLength); dialogpanel.add(lengthTxt);
		dialogpanel.add(enterWidth); dialogpanel.add(widthTxt);
		dialogpanel.add(enterHeight); dialogpanel.add(heightTxt);
		dialogpanel.add(enterDays); dialogpanel.add(daysTxt);
		dialogpanel.add(OKbtn); dialogpanel.add(cancelBtn);
		
		this.newParcelDialog.pack();
		this.newParcelDialog.setSize(300, 300);
		this.newParcelDialog.add(dialogpanel);
		this.newParcelDialog.setVisible(true);
		
		
		
		
	}
	
	public void actionPerformed(ActionEvent e)
	{
		if(e.getSource()== this.addParcelBtn)
		{
			newParcel();			
		}
		
		if(e.getSource() == this.OKbtn)
		{
			//add parcel to list
			String pid = PIDtxt.getText();
			int l = Integer.parseInt(lengthTxt.getText());
			int h = Integer.parseInt(heightTxt.getText());
			int w = Integer.parseInt(widthTxt.getText());
			double kg = Double.parseDouble(weightTxt.getText());
			int days = Integer.parseInt(daysTxt.getText());
			
			
			try
			{
				Parcel p = new Parcel(pid,l,w,h,kg,days);
				parcels.addParcel(p);
				addNewParcelToPanel(p);
				eventlogger.logEvent("New Parcel Added: "+p);
				JOptionPane.showMessageDialog(null, "Parcel entered");
				this.newParcelDialog.setVisible(false);
				
				unprocessedParcel = pid+"\t"+l+" X "+w+" X "+h+"\n";
				//refreshing the textarea
				
				//parcelTextarea.setText(parcelParameter.concat(unprocessedParcel));
				
				
			}
			
			catch(ParcelIDException pie)
			{
				JOptionPane.showMessageDialog(null, pie.getMessage());
			}
			
			catch(ParcelFormatException pfe)
			{
				JOptionPane.showMessageDialog(null, pfe.getMessage());
			}
		}
		
		if(e.getSource() == this.cancelBtn)
		{
			//close dialog
			this.newParcelDialog.setVisible(false);
		}
	
	}
	
	
	
	
}
