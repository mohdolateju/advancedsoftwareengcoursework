package gui;

import java.awt.BorderLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**Class to Display the Main Interface for the application*/
public class MainGui extends JFrame implements ActionListener {

	
	
	//serial version recommended by Eclipse 
	private static final long serialVersionUID = 1L;
	
	/**Constructor to Initialize*/
	public MainGui(){
		this.setLayout(new BorderLayout());
		this.setTitle("Depot System");		
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		setSize(100,600);
		setLocation(100,50);		
				
        pack();
		
		this.setVisible(true);
	}
	
	
	
	
	/**Adds The Customer Panel to the Main Interface
	 * @param Customer Panel as a JPanel Component
	 * @return Customer Panel*/
	public JPanel setCustomerPanel(JPanel customerpanel){		
		this.add(customerpanel, BorderLayout.EAST);
		
		return  customerpanel;
	}
	
	/**Adds The Parcel Panel to the Main Interface
	 * @param Parcel Panel as a JPanel Component
	 * @return Parcel Panel*/
	public JPanel setParcelPanel(JPanel parcelpanel){		
		this.add(parcelpanel, BorderLayout.WEST);
		
		return  parcelpanel;
	}
	
	/**Adds The Parcel Panel to the Main Interface
	 * @param Parcel Panel as a JPanel Component
	 * @return Parcel Panel*/
	public JPanel setWorkerPanel(JPanel workerspanel){		
		this.add(workerspanel, BorderLayout.CENTER);		
		
		return  workerspanel;
	}


	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}
	
}
