package gui;


import main.EventsLogger;
import main.Worker;
import gui.WorkerPanel ;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JOptionPane;





public class WorkerPanelController {
	private Worker anotherworker;
	private WorkerPanel view;
	private String newWorkerName;
	private String newWorkerID;	
	private EventsLogger eventlogger=EventsLogger.getInstance();
	
	public WorkerPanelController(Worker w) {
		anotherworker = w;
		view = new WorkerPanel(w);
		view.addWorkerBtnListener(new WorkerController());
		view.addprocessParcelBtnListener(new WorkerController());
		view.addprocessClearBtnListener(new WorkerController());
		view.cancelButtonListener(new WorkerController());
		view.okButtonListener(new WorkerController());
		view.WorkerBreakBtnListener(new WorkerController());
	
	}
		
    
    public WorkerPanel getView() {
		return view;
	}


	public void setView(WorkerPanel view) {
		this.view = view;
	}


	/**Nested Class to Handle User Events*/
	class WorkerController  implements ActionListener
    {
		//ArrayList of Threads
		private ArrayList<Thread> ThreadList=new ArrayList<Thread>();
		
    	@Override
    	public void actionPerformed(ActionEvent e) {
    		
    				//User the Process Parcel Button Create threads and start processing
    				if(e.getSource()==view.getProcessParcelBtn()){
    					
    					view.getProcessParcelBtn().setEnabled(false);
    					creatAllThreads();    					
    					startAllThreads();    					
    					//Write event to log
    					eventlogger.logEvent("Processing Started");
    					view.update();    					
    					}
    					
    				//If OK Button From Dialog is Clicked Add Worker to JPanel 
    				if(e.getSource()==view.getOkButton()){
    					
    					//Adding new Customer to the Customer List
    					newWorkerName=view.getNameTextField().getText().trim();
    					newWorkerID=view.getWorkerIDTextField().getText().trim();
    					
    					//TextField is empty Display Error message
    					if (newWorkerName.isEmpty())
    					{    						
    						JOptionPane.showMessageDialog(null,
    								"Please Fill All Fields!",
    							    "Add Worker Error!",
    							    JOptionPane.ERROR_MESSAGE);
    					}else{
    						anotherworker.setConcreteworkers(newWorkerName);
    						eventlogger.logEvent("New Worker Added: "+newWorkerName+" "+newWorkerID+"\n");
    					}
    					
    					
    					
    					view.getDialog().setVisible(false);
    				}
    				
    				//If CANCEL Button From Dialog is Click 
    				if(e.getSource()==view.getCancelButton()){
    					view.getDialog().setVisible(false);
    				}
    				
    				//If Clear button is clicked
    				if(e.getSource()==view.getClearBtn()){
    					view.getParcelTextarea().setText(" ");
    					eventlogger.logEvent("clear functionality has been invoked!!"+"\n");
    				}
    				
    				//If Add worker Button is clicked
    				if(e.getSource()==view.getAddWorkerBtn()){
    					view.addWorkerDialog();
    					eventlogger.logEvent("Addition of new worker has been invoked!!"+"\n");
    				}
    				
    				if(e.getSource()==view.getWorkerBreakBtn()){
    										
    				}
    		
    	}
    	
    	//Creates a thread for each worker in the worker list
    	public void creatAllThreads(){
    		for(int i=0; i<anotherworker.getConcreteworkers().size();i++){
    			ThreadList.add(new Thread(anotherworker));    			
    		}
    	}
    	
    	//Starts all the thread in the thread list
    	public void startAllThreads(){
    		for(int i=0;i<ThreadList.size();i++){
    			((Thread) ThreadList.get(i)).start();
    		}
    	}
    	
    	
    }
   }


