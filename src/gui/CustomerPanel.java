package gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Observable;
import java.util.Observer;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.JPanel;
import exceptions.DuplicateCustomerException;
import exceptions.InvalidCustomerException;
import base.Customer;
import lists.CustomerList;
import main.EventsLogger;

/**Represents A Customer JPanel Component*/
public class CustomerPanel implements SwingConstants, ActionListener,Observer {
			
	private CustomerList customers;
	
	private JPanel customerPanel;
	private JButton addCustomerButton;
	private JButton cancelButton;
	private JButton okButton;
	private JDialog dialog;
	private JPanel listPanel;
	
	//Last Line Number of the GUI
	private int lineNo;
	
	//components of the AddCustomer Dialog
	private JTextField nameTextField;
	private JTextField parcelIDTextField;
	
	//The Next Sequence Number
	private int lastAvailSeqNo;
	
	//Reference to the text fields
	private ArrayList<JTextField> textfields;
	
	//A Hash map Reference to the Customer List GUI components
	private HashMap <Integer,JTextField> customerSeqNoTextFields=new HashMap <Integer,JTextField>();
	private HashMap <String,JTextField> customerNameTextFields=new HashMap <String,JTextField>();
	private HashMap <String,JTextField> customerParcelIDTextFields=new HashMap <String,JTextField>();

	//private static Logger logger =Logger.getLogger("AppsLog");
    //FileHandler fh;
	private EventsLogger eventlogger=EventsLogger.getInstance();
	
	/**Initialises the Customer panel from a CustomerList
	 * @param Customer List
	 */
	public CustomerPanel(CustomerList customers){
		
		this.customers=customers;
		textfields=new ArrayList<JTextField>();
		this.customerPanel=new JPanel(new GridBagLayout());
		//Generating NextSequence Number
		lastAvailSeqNo=customers.getCustomerList().getLast().getSeqNo();
		++lastAvailSeqNo;
	}


	/**Adds A New Customer To the List Panel
	   @param New Customer Object*/
	private void addNewCustomerToPanel(Customer customer){
		
		//getting details of the customer
		int seqNo=customer.getSeqNo();
		String name=customer.getName();
		String parcelID=customer.getParcelID();
							
		//displaying the sequence number of the customer in a disabled text field
		JTextField customerSeqNo=new JTextField();
		customerSeqNo.setHorizontalAlignment(CENTER);
		customerSeqNo.setText(" "+seqNo+" ");
		customerSeqNo.setEnabled(false);
		customerSeqNo.setDisabledTextColor(Color.BLACK);
		GridBagConstraints customerConstraints=new GridBagConstraints();
		customerConstraints.fill=GridBagConstraints.HORIZONTAL;
		customerConstraints.gridx=0;			
		customerConstraints.gridy=lineNo;
		customerConstraints.ipadx=12;
		listPanel.add(customerSeqNo, customerConstraints);
		
		textfields.add(customerSeqNo);
		//creating a reference to the text field
		customerSeqNoTextFields.put(seqNo, customerSeqNo);
		
		//displaying the customer name of the customer in a disabled text field
		JTextField customerName=new JTextField();
		customerName.setHorizontalAlignment(CENTER);
		customerName.setText(name);
		customerName.setEnabled(false);
		customerName.setDisabledTextColor(Color.BLACK);
		customerConstraints.gridx=1;
		listPanel.add(customerName, customerConstraints);
		
		textfields.add(customerName);
		//creating a reference to the text field
		customerNameTextFields.put(name, customerName);
		
		//displaying the parcel id of the customer in a disabled text field
		JTextField customerParcelID=new JTextField();
		customerParcelID.setHorizontalAlignment(CENTER);
		customerParcelID.setText(parcelID);
		customerParcelID.setEnabled(false);				
		customerParcelID.setDisabledTextColor(Color.BLACK);
		customerConstraints.gridx=2;			
		listPanel.add(customerParcelID, customerConstraints);
		
		textfields.add(customerParcelID);
		//creating a reference to the text field
		customerParcelIDTextFields.put(parcelID, customerParcelID);
	
		//Line number increased by 1
		lineNo++;
	}
	
	/** Gets the Customer Panel as a JPanel Component
	 * @return Customer JPanel Component*/
	public JPanel getPanel(){
		
		//Setting the Title of the JPanel as a JLabel
		JLabel title=new JLabel("CUSTOMERS LIST", CENTER);
		GridBagConstraints titleConstraints=new GridBagConstraints();	
		titleConstraints.fill=GridBagConstraints.HORIZONTAL;
		titleConstraints.gridx=0;
		titleConstraints.gridy=0;
		titleConstraints.gridwidth=3;
		titleConstraints.ipady=5;				
		this.customerPanel.add(title, titleConstraints);
		
		//Setting the Sequence Number Label 
		JLabel SeqNoTitle=new JLabel("SEQ NO");
		GridBagConstraints seqNoConstraints=new GridBagConstraints();
		seqNoConstraints.gridx=0;
		seqNoConstraints.gridy=1;
		this.customerPanel.add(SeqNoTitle, seqNoConstraints);
		
		//Setting the Customer Name Label
		JLabel customersNameTitle=new JLabel("CUSTOMER'S NAME", CENTER);
		GridBagConstraints customersNameConstraints=new GridBagConstraints();	
		customersNameConstraints.gridx=1;
		customersNameConstraints.gridy=1;
		customersNameConstraints.ipadx=8;
		this.customerPanel.add(customersNameTitle, customersNameConstraints);
		
		//Setting the Parcel ID Label
		JLabel parcelIDTitle=new JLabel("PARCEL ID", CENTER);
		GridBagConstraints parcelIDConstraints=new GridBagConstraints();
		parcelIDConstraints.fill=GridBagConstraints.HORIZONTAL;
		parcelIDConstraints.gridx=2;
		parcelIDConstraints.gridy=1;
		parcelIDConstraints.ipadx=5;
		this.customerPanel.add(parcelIDTitle, parcelIDConstraints);
		
		//JPanel for the List of Customers
		listPanel=new JPanel(new GridBagLayout());
		
		//The line number for the start of the Customer details used in the loop
		lineNo=3;
		
		//Loop for displaying all customers
		for(Customer customer: customers.getCustomerList()){
			
			addNewCustomerToPanel(customer);
		}
		
		//list panel is put in the a scroll pane to make it scrollable
		JScrollPane ListScrollpane=new JScrollPane(listPanel);
		ListScrollpane.setPreferredSize(new Dimension(250,400));
		ListScrollpane.setMinimumSize(new Dimension(250,200));
		
		//setting rules for the List scroll pane
		GridBagConstraints ListConstraints=new GridBagConstraints();
		ListConstraints.fill=GridBagConstraints.HORIZONTAL;
		ListConstraints.gridx=0;
		ListConstraints.gridy=2;
		ListConstraints.gridwidth=3;
		
		this.customerPanel.add(ListScrollpane,ListConstraints);
		
		//Adding the "new customer" button to 
		this.addCustomerButton=new JButton("ADD NEW CUSTOMER");
		this.addCustomerButton.setHorizontalAlignment(CENTER);
		this.addCustomerButton.addActionListener(this);
		GridBagConstraints addCustomerConstraint=new GridBagConstraints();
		addCustomerConstraint.fill=GridBagConstraints.HORIZONTAL;
		addCustomerConstraint.gridx=0;
		addCustomerConstraint.gridy=3;
		addCustomerConstraint.gridwidth=3;
				
		this.customerPanel.add(this.addCustomerButton,addCustomerConstraint);
				
		return customerPanel;
	}		

	/**Method to respond to events on Customer Panel*/
	@Override
	public void actionPerformed(ActionEvent event) {
		
		//User clicks Add Customer Button Display Add Customer Dialog
		if(event.getSource()==this.addCustomerButton){
			addCustomerDialog();			
		}
		
		//If OK Button From Dialog is Clicked Add Customer to CustomerList and JPanel 
		if(event.getSource()==this.okButton){
			//Adding new Customer to the Customer List
			String newCustomerName=nameTextField.getText();
			String newParcelID=parcelIDTextField.getText();
			
			try {				
				Customer newCustomer=new Customer(lastAvailSeqNo,newCustomerName,newParcelID);
				customers.addCustomer(newCustomer);		
				eventlogger.logEvent("New Customer Added: "+newCustomer);
				update(null, newCustomer);				
				
			} catch (DuplicateCustomerException dupex) {				
				//Dialog for exception
				JOptionPane.showMessageDialog(null,
						dupex.getMessage(),
					    "Add Customer Error!",
					    JOptionPane.WARNING_MESSAGE);				
			} catch (InvalidCustomerException icex) {				
				//Dialog for exception
				JOptionPane.showMessageDialog(null,
					    icex.getMessage(),
					    "Add Customer Error!",
					    JOptionPane.WARNING_MESSAGE);
			}
			
			this.dialog.setVisible(false);
		}
		
		//If CANCEL Button From Dialog is Click 
		if(event.getSource()==this.cancelButton){
			this.dialog.setVisible(false);
		}
			
	}
	
	/**Displays Dialog For Adding Customer's Details*/
	public void addCustomerDialog(){		
		this.dialog=new JDialog(new JFrame(), "Add Customer", true);
		
		//Panel to Customise Dialog
		JPanel dialogpanel=new JPanel(new GridBagLayout());
		
		//Configuring Constraints
		GridBagConstraints dialogConstraints=new GridBagConstraints();			
		dialogConstraints.gridx=0;
		dialogConstraints.gridy=0;
		dialogConstraints.gridwidth=2;
		
		//Adding Message Label
		JLabel message=new JLabel("Please Enter Customer Details");
		dialogpanel.add(message, dialogConstraints);
		
		//Adding Name Label
		JLabel nameLabel=new JLabel("Name");
		dialogConstraints.gridx=0;
		dialogConstraints.gridy=1;
		dialogConstraints.gridwidth=1;
		dialogpanel.add(nameLabel, dialogConstraints);
		
		//Adding Name Text Field
		nameTextField=new JTextField();
		dialogConstraints.fill=GridBagConstraints.HORIZONTAL;
		dialogConstraints.gridx=1;
		dialogConstraints.gridy=1;		
		dialogpanel.add(nameTextField, dialogConstraints);
		
		//Adding ParcelID Label
		JLabel parcelIDLabel=new JLabel("Parcel ID");
		dialogConstraints.gridx=0;
		dialogConstraints.gridy=2;
		dialogpanel.add(parcelIDLabel, dialogConstraints);
		
		//Adding ParcelID Text Field
		parcelIDTextField=new JTextField();
		dialogConstraints.gridx=1;
		dialogConstraints.gridy=2;
		dialogpanel.add(parcelIDTextField, dialogConstraints);
		
		//Adding Cancel Button
		this.cancelButton=new JButton("CANCEL");
		cancelButton.addActionListener(this);
		dialogConstraints.gridx=0;
		dialogConstraints.gridy=3;		
		dialogpanel.add(this.cancelButton, dialogConstraints);
		
		//Adding Adding OK Button
		this.okButton=new JButton("OK");
		this.okButton.addActionListener(this);
		dialogConstraints.gridx=1;
		dialogConstraints.gridy=3;		
		dialogpanel.add(this.okButton, dialogConstraints);
		
		//Adding panel to Dialog				
		this.dialog.add(dialogpanel);
		
		//Final Dialog Configuration
		this.dialog.pack();
		this.dialog.setSize(250, 150);
		this.dialog.setLocationRelativeTo(null);
		this.dialog.setVisible(true);	
	}

	/**Adds the New Customer to the JPanel
	 * @param Observable class(null recommended), New Customer Added*/
	@Override
	public void update(Observable arg0, Object arg1) {		
		Customer newcustomer=customers.getCustomerList().getLast();		
		addNewCustomerToPanel(newcustomer);		
		++lastAvailSeqNo;
	}
	
	/**
	 *Updates The Customer TextFields If a Customer has been processed The Textfield's Background turns Green 
	 */
	public synchronized void updateProcessedCustomers(){
		//Update Background Color of Each Sequence Number Textfield if it isn't in the customer object or is already Green
		for(JTextField seqNoTextField: customerSeqNoTextFields.values()){
			int seqNoValue=Integer.parseInt(seqNoTextField.getText().trim());
			
			if(seqNoTextField.getBackground()==Color.GREEN){
				//break;
			}else if(customers.searchBySeqNo(seqNoValue)==null){
				seqNoTextField.setBackground(Color.GREEN);
			}
		}
		
		//Update Background Color of Each Name Textfield if it isn't in the customer object or is already Green
		for(JTextField nameTextField: customerNameTextFields.values()){
			String nameValue=nameTextField.getText();
			if(nameTextField.getBackground()==Color.GREEN){
				//break;
			}else if(customers.searchByName(nameValue)==null){
				nameTextField.setBackground(Color.GREEN);
			}
		}
		
		//Update Background Color of Each Parcel Textfield if it isn't in the customer object or is already Green
		for(JTextField parcelIDTextField: customerParcelIDTextFields.values()){
			String parcelIDValue=parcelIDTextField.getText();
			if(parcelIDTextField.getBackground()==Color.GREEN){
				//break;
			}else if(customers.searchByParcelID(parcelIDValue)==null){
				parcelIDTextField.setBackground(Color.GREEN);
			}
						
		}
	}
}
