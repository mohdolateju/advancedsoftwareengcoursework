package main;

import lists.CustomerList;
import lists.ParcelList;
import base.Customer;
import base.Parcel;

import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import patterns.WorkerObserver;
import patterns.WorkerSubject;

/** This Class initializes all the Parcels and Queue of Customers and Processed them*/
public class Worker extends Thread implements WorkerSubject{
	private ParcelList allParcels;
	private CustomerList allCustomers;
	private String report;
	private int noServed=0;
	private int noNotServed=0;
	private String status;
	private String file;	
	private Random randomGenerator;
	
	//ArrayList of Workers
	private ArrayList<String> concreteworkers = new ArrayList<String>();
	
	//Event Logger Instance
	private EventsLogger eventlogger=EventsLogger.getInstance();
		
	//Array list of worker observers
	private ArrayList<WorkerObserver>workerobservers;
	
	
	/**Initializes the Queue of Customers and All The Parcels
	 * @param Parcel List, Customer List
	 */
	public Worker(ParcelList allParcels, CustomerList allCustomers) {		
		this.allParcels = allParcels;
		this.allCustomers = allCustomers;
		workerobservers=new ArrayList<WorkerObserver>();
		concreteworkers.add("****John Mathew****");
		
		randomGenerator = new Random();
	}
	
	public String getReport() {
		return report;
	}

	public void setReport(String report) {
		this.report = report;
	}

	
	 public ArrayList<String> getConcreteworkers() {
		return concreteworkers;
	}

	public void setConcreteworkers(String concreteworkers) {
		this.concreteworkers.add(concreteworkers);
	}

	public String anyItem()
	    {
	        int index = randomGenerator.nextInt(concreteworkers.size());
	        String item = concreteworkers.get(index);
	       // System.out.println("Managers 's choice for worker this week" + item);
	        eventlogger.logEvent("Worker selected for processing" + item);
	        return item;
	    }
	
	public CustomerList getAllCustomers() {
		return allCustomers;
	}

	public void setAllCustomers(CustomerList allCustomers) {
		this.allCustomers = allCustomers;
	}

	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	/**Processes and Displays A Parcels From the Parcel List
	 * @param Sequence Number
	 */
	public synchronized void processParcel(String seqNo) {
		//process the parcel here
		
		Parcel parcel = allParcels.findParcelById(seqNo.trim());
		if (parcel != null) 
		{
			if(parcel.getParcelID().charAt(0)=='A')
			{report += parcel.toString()+", fees: "+parcel.applyDiscount()+" dhms,AFTER DISCOUNT status: collected \n\n";}
			else report += parcel.toString()+", fees: "+parcel.calculateFee()+" dhms, status: collected \n\n";
			
			this.noServed++;
		}
		else {
			report +="Sorry, Parcel with ParcelId "+seqNo+" is not present in Depot for collection \n\n";
			this.noNotServed++;
		}
				
	}
	
	/**Processes and Displays the parcel id from the next Customer
	 *@return Customers ID
	 */
	public synchronized String processCustomer() {
		//process the parcel here
		
		Customer customer = allCustomers.processNextCustomer() ;	
		report += "Kindly wait, currently Processing Parcel "+customer.getParcelID()+" by worker "+anyItem()+"\n";
		
		//This needs to be further modified to print for customers whose parcel are not in depot
		report += customer.toString()+"\n";
		
		String parcelID =customer.getParcelID();
		
		return parcelID;
		
	}
	
	/**Writes A report to a text file 
	 * @param filename, String to write to report*/
	public void writeToFile(String filename, String report)
	{
		FileWriter fw;
		try
		{
			fw = new FileWriter(filename);
			fw.write(report);
			fw.close();
		}
		 //message and stop if file not found
		catch(FileNotFoundException fnf) {
			System.out.println(filename + "not found");
			
			System.exit(0);
		}
	
		catch(IOException ioe){
		    ioe.printStackTrace();
			System.exit(1);
		}
	}
	
	//Run method for the Thread 
	@Override
	public void run()
	{
		file= "Output.txt";
		report =allCustomers.getCustomerList().size()+ " Customers have been waiting in the queue.\n";
		report += "Started Processing Customer Queue \n\n";
							
		while (allCustomers.hasCustomer()) {
			
			
			try {
				//Time Slicing, thread sleeps for 2 seconds before processing
				Thread.sleep(2000);
				
				//Process Customer 
				String parcel=processCustomer();
				
				//Process Parcel
				processParcel(parcel);
				
				//Print and Log progress
				System.out.println("CUSTOMER WITH PARCEL "+parcel+" PROCESSED");
				eventlogger.logEvent("CUSTOMER WITH PARCEL "+parcel+" PROCESSED");
				
				//Notify all views
				notifyObservers();
			} catch (InterruptedException e) {				
				e.printStackTrace();
			}			
			
		}
		
		//Get status and print to log file
		this.status=this.noServed+" Parcels and Customers have been Processed";
		status+="\n Check Output.txt File For Details.";
		eventlogger.logEvent(this.status);
		
		if(noNotServed>0){
			this.status=this.status+", "+noNotServed+" Customer(s) not Processed";
			eventlogger.logEvent(this.status);
		}
		
		//Adding status to Report
		report+=status;
		
		
		writeToFile(file,report);				
	}
	
	/**Register Worker Observer to ObserverList
	 * @param Worker Observer*/
	@Override
	public void registerObserver(WorkerObserver Observer) {	
		workerobservers.add(Observer);
	}

	/**Remove Worker Observer from ObserverList
	 * @param Worker Observer*/
	@Override
	public void removerObserver(WorkerObserver observer) {		
		workerobservers.remove(observer);
	}

	/**Notify all Worker Observers in the ObserverList*/
	@Override
	public void notifyObservers() {		
		for(WorkerObserver observer:workerobservers){
			observer.update();
		}
	}

	
}
