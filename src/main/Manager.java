package main;

import gui.CustomerPanel;
import gui.MainGui;
import gui.ParcelPanel;
import gui.WorkerPanelController;
import lists.CustomerList;
import lists.ParcelList;
import java.io.IOException;  

/**The Class that Processes the Parcels of the customers*/
public class Manager
{
	
	private Worker worker;   
    private ParcelList allParcels;
    private CustomerList allCustomers;
    private int noOfAddedCustomers;    
    private static EventsLogger eventlogger=EventsLogger.getInstance();  
   
    /**Initializes all required components before processing Parcels*/
    public Manager()
    {
    	allParcels = new  ParcelList();
    	allCustomers = new CustomerList();
    	
    }
    
    /**Initializes list of Parcels and Queue of Customers and Displays the GUI
     *@throws InputOutputException 
     */
    public synchronized void run()  {
    	allParcels.readDepotFile("Depot.txt","Depot");
    	
    	noOfAddedCustomers=allCustomers.addCustomersFromFile("Customer.txt");
    	
    	//Initializing Main Interfaces
    	MainGui main=new MainGui();
    	
    	//Creating Customer JPanel from Customer List 
    	CustomerPanel customerPanel= new CustomerPanel(allCustomers);
    	
    	//Adding the Customer Panel to the Main Interface
    	main.setCustomerPanel(customerPanel.getPanel());
    	
    	//Creating Parcels JPanel from Parcel List 
    	ParcelPanel parcelPanel= new ParcelPanel(allParcels);
    	
    	//Adding the Parcels Panel to the Main Interface
    	main.setParcelPanel(parcelPanel.getPanel());
    	    	    	
    	System.out.println(noOfAddedCustomers+" customers are in the queue to be processed.");
    	eventlogger.logEvent(noOfAddedCustomers+" customers are in the queue to be processed.");
    	
    	System.out.println("---------------------------------------------------------------");
    	eventlogger.logEvent("---------------------------------------------------------------");
    	
    	
    	worker = new Worker(allParcels, allCustomers);
 
     	eventlogger.logEvent("generated the Output.txt File For Details");
    	    	    	
    	//Creating and Adding the Workers Panel to the Main Interface
    	
    	WorkerPanelController workersPanel=new WorkerPanelController(worker);
    	workersPanel.getView().WritetoGUI(worker);
    	main.setWorkerPanel(workersPanel.getView().getPanel());
    	
    	//Making the Main Interface automatically select the right size of the Window
    	main.pack(); 
    	
    	//Update the GUI to show which customer have been processed
    	while(allCustomers.getCustomerList().size()>0){
    		
    		//Sleep To Prevent Concurrency Error
    		try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		customerPanel.updateProcessedCustomers();    		
    	}
    	//Last Customer in the Panel should be updated
    	customerPanel.updateProcessedCustomers();
    }
    
    /**start the program*/
    public static void main (String args[]) throws IOException {
    	    
        // the following statement is used to log any messages  
        eventlogger.logEvent("Welcome to the Depot System History");                  
    	Manager manager = new Manager();       
    	manager.run();
    	
    }
    
}

