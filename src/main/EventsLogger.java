package main;

import java.io.IOException;  
import java.util.logging.FileHandler;  
import java.util.logging.Level;  
import java.util.logging.Logger;  
import java.util.logging.SimpleFormatter; 

public class EventsLogger {
	
	private static EventsLogger instance;
	private static Logger logger;
    private FileHandler fh; 

	private EventsLogger() {
		logger =Logger.getLogger("AppsLog");
			try {
				 fh = new FileHandler("AppsLogFile.log");  
		         logger.addHandler(fh);  
		         logger.setLevel(Level.ALL);  
		         SimpleFormatter formatter = new SimpleFormatter();  
		         fh.setFormatter(formatter);
			} catch (SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}           
	}
	
	public synchronized static EventsLogger getInstance(){
		if(instance==null){
			instance=new EventsLogger();
		}
		
		return instance;
	}

	public synchronized void  logEvent(String event){
		logger.info(event);
	}	
}
