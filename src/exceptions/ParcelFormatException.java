package exceptions;

@SuppressWarnings("serial")
/**Parcel Format Exception thrown For a Wrong Parcel attribute*/
public class ParcelFormatException extends Exception {

	/**Message For a Wrong Parcel attribute
	 * @param Invalid attribute
	 */
	public ParcelFormatException(String message)
	{super("Parcel dimension values, check: " + message);}
}
