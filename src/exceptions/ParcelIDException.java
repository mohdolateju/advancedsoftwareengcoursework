package exceptions;

@SuppressWarnings("serial")
/**Parcel ID Exception Class used to Check for Correct Parcel ID*/
public class ParcelIDException extends Exception {

	/**Message Displayed if Format if Parcel ID is incorrect
	 *@param Invalid Attribute
	 */
	public ParcelIDException(String message){
	super("Parcel ID format incorrect: " + message);
	}
}
