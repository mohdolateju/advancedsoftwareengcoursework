package exceptions;

@SuppressWarnings("serial")
/**Exceptions thrown when an Invalid Customer detail is added*/
public class InvalidCustomerException extends Exception{

	/**Constructor to Display the Cause of Exception*/
	public InvalidCustomerException(String attribute){
		super("Customer could not be created because "+attribute+" is invalid");		
	}
}
