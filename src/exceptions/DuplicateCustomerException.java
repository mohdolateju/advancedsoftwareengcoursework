package exceptions;

@SuppressWarnings("serial")
/**Exceptions thrown when a Customer already exists in a Customer List*/
public class DuplicateCustomerException extends Exception{

	/**Constructor to Display the Cause of Exception*/
	public DuplicateCustomerException(String attributeType, String attribute){
		super("Customer with "+attributeType+"=> "+attribute+" is already in the Custumer List");		
	}
}
