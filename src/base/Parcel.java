package base;


import exceptions.ParcelFormatException; 
import exceptions.ParcelIDException;

/**Represents a Parcel*/
public class Parcel implements Comparable <Parcel> {

	private String parcelID;
	private int length,width,height;//parcel dimensions
	private double weight;
	private int daysInDepot;
	
	/** Creating a Parcel Object
	 * @param Parcel ID, Parcel Length, Parcel Width, Parcel Height, Parcel Weight, Day In Depot
	 * @throws ParcelIDException, ParcelFormatException
	 */
	public Parcel(String parcelID,int length,int width,int height,double weight,int dayInDepot) throws ParcelIDException, ParcelFormatException{
		switch(parcelID.charAt(0)){
		case 'A':this.parcelID = parcelID;
				break;
		case 'B':this.parcelID = parcelID;
				break;
		case 'C':this.parcelID = parcelID;
				break;
		case 'D':this.parcelID = parcelID;
				break;
		default: throw new ParcelIDException("ParcelID :"+parcelID);		
		
		
		}
		
		//length can't be zero or negative
		if(length<=0){throw new ParcelFormatException("Length at "+parcelID);}
		else
			this.length = length;
		
		//width can't be zero or negative
		if(width<=0){throw new ParcelFormatException("Width at "+parcelID);}
		else
			this.width = width;
		
		//Height can't be zero or negative
		if(height<=0){throw new ParcelFormatException("Height at "+parcelID);}
		else
			this.height = height;
		
		//Weight can't be zero or negative
		if(weight<=0){throw new ParcelFormatException("Weight at "+parcelID);}
		else
			this.weight = weight;
		
		
		this.daysInDepot = dayInDepot;
	}
	
	/**Gets the Parcel ID
	 * @return Parcel ID*/
	public String getParcelID(){
		return this.parcelID;
		}
	
	/**Gets the Parcel Dimensions
	 * @return Parcel's Dimensions*/
	public String getDimension(){
		return this.length+" X "+this.width+" X "+this.height;
		}
	
	/**Gets the Parcel Volume
	 * @return Parcel's Volume*/
	public int getVolume(){
		return this.length*this.width*this.height;
		}
	
	/**Gets the Parcel Weight
	 * @return Parcel's Weight*/
	public double getWeight(){
		return this.weight;
		}
	
	/**Gets the Number of Day The Parcel
	 * @return Number of Day The Parcel*/ 
	public int getDaysInDepot(){
		return this.daysInDepot;
		}
	
	/**Sets the Parcel ID
	 * @param Parcel ID*/
	public void setParcelID(String parelID){
		this.parcelID = parelID;
		}
	
	/**Sets the Parcel Dimension
	 * @param Parcel Length, Parcel Width,Parcel Height
	 */
	public void setDimension(int length,int width,int height){
		this.length = length;
		this.width = width;
		this.height = height;
	}
	
	/**Sets the Parcel Weight
	 * @param Parcel Weight*/
	public void setWeight(double weight){
		this.weight = weight;
		}
	
	/**Sets the Parcel Days
	 * @param Parcel Days*/
	public void setDays(int days){
		this.daysInDepot = days;
		}
	
	/**Gets the Type of Parcel(First Character of the Parcel ID)
	 * @return Parcel Type as a Costumer
	 */
	public char getParcelType(){
		return this.parcelID.charAt(0);		
	}
	
	/**Calculates and Gets the Total Fee of The Parcel 
	 * dimension fee: [parcel dimension] X [*AED]
	 * day fee: [days in depot] X [*fills]
	 * total fee: [dimension fee] + [day fee]
	 * @return Total Parcel Fee
	 */
	public double calculateFee(){
		double dimensionFee=0.0;
		double daysFee=0.0;
		double totalFee=0.0;
		
		//for parcel greater than 50metre cube
		if(this.getVolume() > 50){
			dimensionFee = this.getVolume()*15;
			daysFee = (double)this.getDaysInDepot()*0.21;
			totalFee = dimensionFee+daysFee;}
		
		//for parcel between 21-50metre cube
		if(this.getVolume() <= 50 && this.getVolume()>= 21){
			dimensionFee = this.getVolume()*10;
			daysFee = (double)this.getDaysInDepot()*0.30;
			totalFee = dimensionFee+daysFee;}
		
		//for parcel between 11-20metre cube
		if(this.getVolume() <= 20 && this.getVolume()>= 11){
			dimensionFee = this.getVolume()*7;
			daysFee = (double)this.getDaysInDepot()*0.35;
			totalFee = dimensionFee+daysFee;}
		
		//for parcel less than 10metre cube
		if(this.getVolume() <= 10){
			dimensionFee = this.getVolume()*3;
			daysFee = (double)this.getDaysInDepot()*0.50;
			totalFee = dimensionFee+daysFee;}
		
		return totalFee;
	}
	
	/**
	 * Gets the Discount Fee from the Parcel
	 * @return Discount Parcel Fee
	 */
	public double applyDiscount(){
		
		double originalFee=0.0;
		double discountValue=0.0;
		
		originalFee = this.calculateFee();
		discountValue = originalFee * 0.08;
		
		return (originalFee-discountValue);
	}

	/**
     * Compare this object against another, for the purpose
     * of sorting. The fields are compared by id.
     * @param otherDetails The details to be compared against.
     * @return a negative integer if this id comes before the parameter's id,
     *         zero if they are equal and a positive integer if this
     *         comes after the other.
     */

    public int compareTo(Parcel otherparcel)
    {
        return parcelID.compareTo(otherparcel.getParcelID());
    }    

    
    /**Gets the Parcel's Details as a String
	 * @return A string of Parcel Details.
	 * */
    public String toString() {
		
		String report = "Parcel ID:  " + getParcelID()+" , DaysInDepot: "+getDaysInDepot()+ " days , having weight:  " + getWeight()+" kg , with Dimension: "+getDimension();
		
	    	return report;
	    }	
	
}
