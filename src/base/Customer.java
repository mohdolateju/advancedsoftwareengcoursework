package base;

import exceptions.InvalidCustomerException;

/**Represents a Customer*/
public class Customer implements Comparable<Customer>{
	
	private String name;
	private int seqNo;
	private String parcelID;
	
	/**Constructor for Creating a Customer Object
	 * @throws InvalidCustomerException 
	 * @param(Sequence Number, Customer Name, Parcel ID) 
	 */
	public Customer(int seqNo, String name, String parcelID) throws InvalidCustomerException{
		
		if(seqNo<=0){
			throw new InvalidCustomerException("Sequence No");
		}else {
			this.seqNo=seqNo;
			}
		
		if(name.trim().isEmpty()){
			throw new InvalidCustomerException("Name");
		}else{
			this.name=name;
			}
		
		if(parcelID.trim().isEmpty()){
			throw new InvalidCustomerException("Parcel ID");
		}else{
			this.parcelID=parcelID;
			}
		
	}
	
	/**Gets the Customer's Sequence Number
	 * @return Sequence Number*/
	public int getSeqNo(){
		return seqNo;
	}
	
	/**Sets the Customer's Sequence Number
	 * @param Sequence Number*/
	public void setSeqNo(int seqNo){
		this.seqNo=seqNo;
	}
	
	/**Gets the Customer's Name
	 * @return Customer'Name */
	public String getName(){
		return name;
	}
	
	/**Sets the Customer's Name
	 * @param Name
	 */
	public void setName(String name){
		this.name=name;
	}
	
	/**Gets the Customer's Parcel ID
	 * @return the Customer's Parcel ID
	 */
	public String getParcelID(){
		return parcelID;
	}
	
	/**Sets the Customer's Parcel ID
	 * @param ParcelID
	 */
	public void setParcelID(String parcelID){
		this.parcelID=parcelID;
	}
	
	/**Gets the Customer's Details as a String
	 * @return A string of Customer Details.*/
	@Override
    public String toString()
    {
        return "Sequence No=> "+getSeqNo()+ ", Customer Name=> "+ getName()+ ", Parcel ID=> "+getParcelID(); 
    }
	
	/**Tests to see if all the properties of the Customers are equal
	 * @param Customer to be Compared with
	 * @return TRUE if all properties are equal or FALSE if any property is not equal
	 */
	public boolean equals(Customer anotherCustomer){
		
		int otherseqNo=anotherCustomer.getSeqNo();
		String otherName=anotherCustomer.getName();
		String otherParceID=anotherCustomer.getParcelID();
		
		if(otherseqNo==this.seqNo && this.name.equalsIgnoreCase(otherName) 
				&& this.parcelID.equalsIgnoreCase(otherParceID)){
			return true;
		}else{
			return false;
		}
		
	}
    
	/**Compares the two Customer by using the sequence number
	 * @param Customer to be Compared with 
	 * @return 0 if they are equal, 1 if 1st Customer is greater and -1 if 2nd Customer is greater */
	@Override
	public int compareTo(Customer anotherCustomer) {
		
		int otherseqNo=anotherCustomer.getSeqNo();		
		
		if(this.seqNo==otherseqNo){
			return 0;
		}else if(this.seqNo>otherseqNo){
			return 1;
		}else if(this.seqNo<otherseqNo){
			return -1;
		}
		return 0;
	}
}
