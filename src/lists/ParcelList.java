 package lists;

import java.util.*;
import java.io.*;

import exceptions.ParcelFormatException;
import exceptions.ParcelIDException;
import base.Parcel;
import java.io.IOException;  

/**Class to Maintain a list of Parcels*/
public class ParcelList{

	private static ArrayList<Parcel> parcelList;
	private static int initialCapacity = 20; // assumed initial capacity
	
	/**Initializes the Parcel List*/
	public ParcelList(){
		parcelList = new ArrayList<Parcel>(initialCapacity);
	}

	/**Gets the List of Parcels
	 * @return Customer List as type LinkedList*/
	public ArrayList<Parcel> getParcelList(){		
		return parcelList;
	}
	
	/**Gets The Total Number of Parcels
	 * @return Parcel Number
	 */
	public int getTotalParcels() {
    	return parcelList.size();
    }
    
	/**Gets A Parcel From an Index
	 * @param Index
	 * @return Parcel Object
	 */
    public Parcel getAtIndex(int index) {
        return parcelList.get(index);
    }
	
	/**
	 * This Method Adds A Created PARCEL object to the list
	 * @param Parcel object 
	 */
	public void addParcel(Parcel p){
		parcelList.add(p);
		}
	
	/**
	 * Checks to see if Parcels are present in the Parcel List
	 * @return TRUE if 1 or more Parcels are present, FALSE if Parcel List is empty  
	 */
	public boolean hasParcel() {
    	return parcelList.size() != 0;
    }
	
	/**
	 * Gets and Removes the Next Parcel from the List
	 * @return Returns the Next Parcel in the List  
	 */
	public Parcel processNextParcel() {
    	Parcel parcel =  parcelList.remove(0);
    	return parcel;

    }

	/**
	 * Gets Parcel From A Parcel ID
	 * @param ParcelID
	 * @return Parcel  
	 */
	 public Parcel findParcelById(String parcelId){
	    	for (Parcel parcel : parcelList)
	    	{
	    		if (parcel.getParcelID().equals(parcelId))
	    		{
	    			return parcel;
	    		}
	    	}
	    	return null;   	
	    }
	
	/**
	 * Gets Parcel from a Line of text
	 * @param Parcel Line
	 * @throws ParcelFormatException 
	 * @throws ParcelIDException 
	 */
	private void getParcelfromDepot(String line)throws ParcelIDException,ParcelFormatException {
		try {

			String parts[] = line.split(",");
			
			String pid = parts[0];
			int length = Integer.parseInt(parts[1].trim());
			int width = Integer.parseInt(parts[2].trim());
			int height = Integer.parseInt(parts[3].trim());
			double weight = Double.parseDouble(parts[4].trim());
			int days = Integer.parseInt(parts[5].trim());
			
			
			Parcel p = new Parcel(pid, length, width, height, weight, days);
			
			addParcel(p);
		}

		catch(ParcelIDException pie){
			String error = "Invalid ID match: " + pie.getMessage();
			System.out.println(error);
		}
		catch(ParcelFormatException pef){
			String error = "Invalid element found in list: " + pef.getMessage();
			System.out.println(error);
		}
		
		catch (NumberFormatException e) {
			String error = "Invalid element found in list: " + e.getMessage();
			System.out.println(error);
		}
		
		catch (ArrayIndexOutOfBoundsException air) {
			String error = "Not enough values in  : '" + line+ "' index position : " + air.getMessage();
			System.out.println(error);
			}
	}
	
	/**
	 * Reads a List of Parcels from A File
	 * @param FileName, Where Parcel is stored 
	 */
	public void readDepotFile(String fileName, String type) {
        BufferedReader bufferedReader = null;
              try {
                 bufferedReader = new BufferedReader(new FileReader(fileName));
                 String line = null;
                 while ((line = bufferedReader.readLine()) != null) {
                     if(line!=null && line.length()!=0){
                         if(type.equalsIgnoreCase("Depot"))
                        	 getParcelfromDepot(line);
                     }
                 }
             }
              catch(ParcelFormatException pef){
      			String error = "Invalid element found in list: " + pef.getMessage();
      			System.out.println(error);
      		}
      		catch(ParcelIDException pie){
      			String error = "Invalid ID match: " + pie.getMessage();
      			System.out.println(error);
      		}
              
             catch (FileNotFoundException ex) {
                 System.out.println(fileName+" cannot be found! Please check file name.");
                 System.exit(0);
             } 
             catch (IOException ex) {
                 ex.printStackTrace();
                 System.exit(1);
             } 
             finally {            
                 try {
                     if(bufferedReader != null)
                         bufferedReader.close();
                 } 
                 catch (IOException ex) {
                     ex.printStackTrace();
                     System.exit(1);
                 }
             }
         }
	
	
	/**
	 * Gets all Parcel Details As A String
     * @return All the details As a String
     */
    public String listDetails()
    {
    	StringBuffer allEntries = new StringBuffer();
    	for (Parcel details : parcelList) {
            allEntries.append(""+details.getParcelID()+"\t"+ details.getDimension()+ "\t:Collected");
            allEntries.append('\n');
        }
        return allEntries.toString();
    }
    
   
    

}


