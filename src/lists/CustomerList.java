package lists;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.LinkedList;
import java.util.Scanner;

import patterns.CustomerObserver;
import patterns.CustomerSubject;

import base.Customer;



import exceptions.DuplicateCustomerException;
import exceptions.InvalidCustomerException;

/**Class to Maintain a List of Customers*/
public class CustomerList implements CustomerSubject{

	//Customer List 
	private LinkedList<Customer> customerList;
	
	//List of Observers
	private LinkedList<CustomerObserver> observerList;	
	
	/**Initialises the Customer List and important variables*/
	public CustomerList(){
		customerList=new LinkedList<Customer>();
		observerList=new LinkedList<CustomerObserver>();			
	}
	
	/**Gets the List of Customers
	 * @return Customer List as type LinkedList*/
	public synchronized LinkedList<Customer> getCustomerList(){		
		return customerList;
	}				

	/**Adds a customer to the End of the Customer Linked List
	 * @param Customer Object
	 * @return TRUE if the customer was added, FALSE if the customer already exists and was not added
	 * @throws DuplicateCustomerException 
	 */
	public synchronized boolean addCustomer(Customer customer) throws DuplicateCustomerException{		
		if(this.customerList.contains(customer)){			
			return false;
		}else{
			for(Customer thiscustomer:customerList){
				if(thiscustomer.getSeqNo()==customer.getSeqNo()){
					throw new DuplicateCustomerException("Sequence Number",Integer.toString(customer.getSeqNo()));					
				}else if(thiscustomer.getName().equalsIgnoreCase(customer.getName())){
					throw new DuplicateCustomerException("Customer Name",customer.getName());					
				}else if(thiscustomer.getParcelID().equalsIgnoreCase(customer.getParcelID())){
					throw new DuplicateCustomerException("Parcel ID",customer.getParcelID());					
				}
			}
			this.customerList.addLast(customer);
			notifyObservers();
			return true;			
		}
		
	}
	
	/**
	 * Only Gets the Next Customer from the List
	 * @return Returns the Next Customer  
	 */
	public synchronized Customer getNextCustomer() {
    	Customer customer =  customerList.peekFirst();
    	return customer;

    }
	
	/**
	 * Gets and Removes the Next Customer from the List
	 * @return Returns the Next Customer  
	 */
	public synchronized Customer processNextCustomer() {
    	Customer customer =  customerList.pollFirst();
    	return customer;

    }
	
	/**Removes a customer from the Customer Linked List
	 * @param Customer Object
	 * @return TRUE if the the customer was removed, FALSE if the customer does not exist in the list and was not added
	 */
	public synchronized boolean removeCustomer(Customer customer){		
		if(!customerList.contains(customer)){
			return false;
		}else{
			customerList.remove(customer);
			return true;
			
		}
		
	}
	
	
	/**
	 * Checks to see if Customer are present in the Customer List
	 * @return TRUE if 1 or more Customers are present, FALSE if Customer List is empty  
	 */
	public synchronized boolean hasCustomer() {
    	return customerList.size() != 0;
    }
	
	/**
	 * Adds A List of Customers from specified File
	 * Each Line to be read should be formated as follows => Sequence Number, Customer Name, Parcel ID
	 * @param URI of the File containing the list of Customers
	 * @return Number of Successfully Added Customers 
	 */
	public synchronized int addCustomersFromFile(String FileURI){
			
		int addedcustomerNo=0;
		int readLineNumber=0;
		
		//Start of File reading process
		try{	      
	        File compFile=new File(FileURI);
	        FileReader file=new FileReader(compFile);	        
	        Scanner line = new Scanner(file);
	        
	        //As Far as there is a next line, through the file
	        while(line.hasNextLine()){
	        	 		++readLineNumber;
	        	 		
	        			//Read line and add them as customers 
	        			//If any Exceptions are caught print the message and line number then continue the loop
			        	try {			        		
							Customer newcustomer=addCustomerFromString(line.nextLine());
							addCustomer(newcustomer);							
							addedcustomerNo++;
							
						} catch (InvalidCustomerException icex) {							
							System.out.println("Error! "+icex.getMessage()+" At Line Number "+readLineNumber+" From "+FileURI);							
						} catch (NumberFormatException nex) {							
							System.out.println("Error! "+nex.getMessage()+" At Line Number "+readLineNumber+" From "+FileURI);
						}catch (ArrayIndexOutOfBoundsException arex) {							
							System.out.println("Error! Only "+arex.getMessage()+" Out of 3 Values Found At Line Number "+readLineNumber+" From "+FileURI);
						} catch (DuplicateCustomerException dupex) {
							System.out.println("Error! "+dupex.getMessage()+", Exception Origin is at Line Number "+readLineNumber+" From "+FileURI); 
						}	        	
	            }
	        line.close();
	      }		  
	      catch(FileNotFoundException fnfex){
	    	  System.out.println("Error! No Customer Was Added Because The File '"+FileURI+"' Could Not Be Found.");
	        }
		
		//Return the Number of Customers Added to the CustomerList
		return addedcustomerNo;
		
	}
	
	
	/**
	 * Used to convert a line of text to a Customer Object
	 * The Line to be read should be formated as follows => Sequence Number, Customer Name, Parcel ID  
	 * @param Line of Customer Details
	 * @return Customer Object 
	 */
	private synchronized Customer addCustomerFromString(String line) throws InvalidCustomerException{
		
		 	String[] details = line.split(",");	        
	        int seqNo=Integer.parseInt(details[0].trim());	        
	        String name=details[1].trim();	        
	        String parcelID=details[2].trim();
	        	       
	     return new Customer(seqNo, name, parcelID);	       		
	}
	 
	/**Search For A Customer with Specified Sequence Number
	 * @param Sequence Number
	 * @return Customer 
	 */
	public synchronized Customer searchBySeqNo(int seqNo){
		
		for(Customer customer: customerList){
			if(customer.getSeqNo()==seqNo){
				return customer; 
			}
		}
		
		return null;
	}
	
	/**Search For A Customer with Specified Customer Name
	 * @param Custumer Name
	 * @return Customer 
	 */
	public synchronized Customer searchByName(String name){
		
		for(Customer customer: customerList){
			if(customer.getName().equalsIgnoreCase(name)){
				return customer; 
			}
		}
		
		return null;
	}
	
	/**Search For A Customer with Specified Parcel ID
	 * @param Parcel ID
	 * @return Customer
	 */
	public synchronized Customer searchByParcelID(String parcelID){
		
		for(Customer customer: customerList){
			if(customer.getParcelID().equalsIgnoreCase(parcelID)){
				return customer; 
			}
		}
		
		return null;
	}
	
	/**
	 * Gets all Customers' Details As A String
     * @return All the details As a String
     */
    public synchronized String toString()
    {
    	StringBuffer customerDetails = new StringBuffer();
    	for(Customer details:customerList){
    		customerDetails.append(details+"\n");    		
        }
    	
        return customerDetails.toString();
    }

    /**Registers an Observer from the Observer List(Observer Pattern)
     * @param Customer Observer */
	@Override
	public void registerObserver(CustomerObserver customerObserver) {
		observerList.add(customerObserver);
		
	}

    /**Removes an Observer from the Observer List(Observer Pattern)
     * @param Customer Observer */
	@Override
	public void removerObserver(CustomerObserver customerObserver) {
		observerList.remove(customerObserver);		
	}

	/**Notify's All Observers in the Observer List(Observer Pattern)
     * @param Customer Observer */
	@Override
	public void notifyObservers() {
		for(CustomerObserver customerObserver: observerList){
			customerObserver.update();
		}
		
	}
		
	
	
}
