package patterns;

/*Interface For Subscribing Customer Observers*/
public interface WorkerSubject {
	
	/**Register an Observer to a list of Customers Observers
	 * @param Observer to be registered*/
	public void registerObserver(WorkerObserver Observer);
	
	/**Remove an Observer from a list of Customers Observers
	 * @param Observer to be removed*/
	public void removerObserver(WorkerObserver observer);
	
	/**Notify all Customer Observers*/
	public void notifyObservers();

}
