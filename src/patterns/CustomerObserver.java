package patterns;

/**Interface for Updating an Observer*/
public interface CustomerObserver {
	
	/**Updates an Observer*/
	public void update();

}
