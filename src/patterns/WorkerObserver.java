package patterns;

/**Interface for Updating an Observer*/
public interface WorkerObserver {
	
	/**Updates an Observer*/
	public void update();

}
