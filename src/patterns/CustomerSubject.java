package patterns;

/*Interface For Subscribing Customer Observers*/
public interface CustomerSubject {
	
	/**Register an Observer to a list of Customers Observers
	 * @param Observer to be registered*/
	public void registerObserver(CustomerObserver Observer);
	
	/**Remove an Observer from a list of Customers Observers
	 * @param Observer to be removed*/
	public void removerObserver(CustomerObserver observer);
	
	/**Notify all Customer Observers*/
	public void notifyObservers();

}
